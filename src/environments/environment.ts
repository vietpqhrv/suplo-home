// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBAvCYirfQKqK2QTm-TGAXYH4Je1OJWNno",
    authDomain: "suplo-home-c880c.firebaseapp.com",
    databaseURL: "https://suplo-home-c880c.firebaseio.com",
    projectId: "suplo-home-c880c",
    storageBucket: "",
    messagingSenderId: "9901337556"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
