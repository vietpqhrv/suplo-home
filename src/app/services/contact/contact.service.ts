import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { AngularFirestore } from '@angular/fire/firestore'


@Injectable({
  providedIn: 'root'
})
export class ContactService {
  fireStore;
  userRef;
  db = firebase.firestore();
  constructor(
    private ft: AngularFirestore,
  ) {
    this.fireStore = firebase.firestore();
    this.userRef = this.fireStore.collection('register');
    // /user là folder tổng trên firestore

  }

  postUser() {
    return this.userRef;
  }

}
