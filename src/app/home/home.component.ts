import { Component, OnInit, ViewChild, HostListener, Inject, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ContactService } from '../services/contact/contact.service';
import { OwlCarousel } from 'ngx-owl-carousel';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { DOCUMENT } from '@angular/common';
import { WOW } from 'wowjs/dist/wow.min';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [trigger('fade', [])],
})

export class HomeComponent implements OnInit {

  show: boolean = false;

  form: FormGroup;
  submitted = false;

  @ViewChild('owlElement') owlElement: OwlCarousel;

  carouselOptions = {
    smartSpeed: 1000,
    margin: 25,
    navigation: false,
    dots: false,
    loop: true,
    autoplay: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 2,
        nav: true
      },
      600: {
        items: 2,
        nav: true
      },
      1000: {
        items: 6,
        nav: true,
        loop: false
      },
      1500: {
        items: 6,
        nav: true,
        loop: false
      }
    }
  }

  images = [
    {
      image: "../../assets/brand_img1.png"
    },
    {
      image: "../../assets/brand_img2.png"
    },
    {
      image: "../../assets/brand_img3.png"
    },
    {
      image: "../../assets/brand_img10.png"
    },
    {
      image: "../../assets/brand_img4.png"
    },
    {
      image: "../../assets/brand_img5.png"
    },
    {
      image: "../../assets/brand_img6.png"
    },
    {
      image: "../../assets/brand_img7.png"
    },
    {
      image: "../../assets/brand_img8.png"
    }
  ]

  links = [
    {
      link: "/"
    },
    {
      link: "/"
    },
    {
      link: "/"
    },
    {
      link: "/"
    },
    {
      link: "/"
    },
    {
      link: "/"
    },
    {
      link: "/"
    },
    {
      link: "/"
    },
  ]


  constructor(
    private formBuilder: FormBuilder,
    private ctService: ContactService,
    @Inject(DOCUMENT) document,
  ) {
    new WOW().init(); //chỗ chạy wow
  }


  ngOnInit() {

    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(10)]],
      node: ['', Validators.required],
      company: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      value_money: ['', Validators.required],
      value_project: this.formBuilder.group({
        value_project1: [false, Validators.required],
        value_project2: [false, Validators.required],
        value_project3: [false, Validators.required],
        value_project4: [false, Validators.required],
      })
    });
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset > 20) {
      let element = document.getElementById('header-pc');
      element.classList.add('sticky');
    } else {
      let element = document.getElementById('header-pc');
      element.classList.remove('sticky');
    }
  }


  toggleClass() {
    this.show = !this.show
  }

  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.setData();
    console.log(this.form.value);


    alert('Thông tin của bạn đã được gửi thành công! Suplo sẽ liên hệ với bạn trong 24 giờ')
  }
  value_project;
  setData() {
    const data = {
      name: this.form.value.name,
      phone: this.form.value.phone,
      company: this.form.value.company,
      email: this.form.value.email,
      node: this.form.value.node,
      value_money: this.form.value.value_money,
      value_project: this.form.value.value_project,
    };
    this.ctService.postUser().add(data).then(data => {
      console.log(data);
    });
  }


  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  fun() {
    this.owlElement.next([200])
    //duration 200ms
  }

}
